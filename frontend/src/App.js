import ListPage from './pages/ListPage';
import DetailsPage from './pages/DetailsPage';
import MessageFormPage from './pages/MessageFormPage';
import CreateFormPage from './pages/CreateFormPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/">
          <Route index element={<ListPage />} />
          <Route path="create" element={<CreateFormPage />} />
          <Route path="details/:id" element={<DetailsPage />} />
          <Route path="message/:id" element={<MessageFormPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

