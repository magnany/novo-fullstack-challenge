import React from 'react';
import axios from 'axios';
import List from './List';
import { Link } from "react-router-dom";

export default class ListPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: true, patients: [] }
  }

  async componentDidMount() {
    await this.reloadData();
  }

  async reloadData() {
    this.setState({ loading: true, patients: [] });
    const resp = await axios.get('http://localhost:3001/patient/');
    if (resp.data.success) {
      this.setState({ loading: false, patients: resp.data.result });
    } else {
      // gestion d'erreur lightweight
      alert(JSON.stringify(resp.data.result));
      this.setState({ loading: false, patients: [] });
    }
  }

  render() {
    return (
      <div class="mt-3">
        <h3>Liste de patients</h3>

        <Link to="/create">Créer un patient</Link>

        <hr/>
        { 
          this.state.loading 
          ? <p>Chargement en cours...</p> 
          : <List patients={this.state.patients} />
        }
      </div>
    );
  }
}
