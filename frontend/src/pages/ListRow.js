import { Link } from "react-router-dom";
import { formatDateAndTime } from '../formatters';
import DeleteButton from './DeleteButton';

export default function ListRow(props) {
  const patient = props.patient;

  return (patient.latest_vitals != null)
      ? (<tr>
          <th scope="row">
            <Link to={`/details/${patient._id}`}>
              {patient.last_name}, {patient.first_name}
            </Link>
          </th>
          <td>{formatDateAndTime(patient.latest_vitals.timestamp)}</td>
          <td>{patient.latest_vitals.heartrate} bpm</td>
          <td>{patient.latest_vitals.body_temp}°C</td>
          <td>{patient.latest_vitals.spo2}</td>
          <td>
            <DeleteButton patient_id={patient._id} />
          </td>
        </tr>)
      : (<tr>
          <th scope="row">
            <Link to={`/details/${patient._id}`}>
              {patient.last_name}, {patient.first_name}
            </Link>
          </th>
          <td colSpan="4"><em>aucun signe vital connu</em></td>
          <td>
            <DeleteButton patient_id={patient._id} />
          </td>
        </tr>);
}

