import React from 'react';
import axios from 'axios';
import { withRouter } from '../withRouter'

class MessageFormPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: true, patient: {} }
  }

  async componentDidMount() {
    const id = this.props.router.params.id;
    const resp = await axios.get(`http://localhost:3001/patient/${id}`)
    if (resp.data.success) {
      this.setState({ loading: false, patient: resp.data.result });
    } else {
      // gestion d'erreur lightweight
      alert(JSON.stringify(resp.data.result));
      this.setState({ loading: false, patient: undefined });
    }
  }

  render() {
    return (
      <div>
        {
          this.state.loading
          ? <h1>Envoyer un message</h1>
          : <h1>Envoyer un message à {this.state.patient.details.first_name} {this.state.patient.details.last_name}</h1>
        }
        <hr />
        { 
          this.state.loading 
          ? <p>Chargement en cours...</p> 
          : <p>show form here</p> 
        }
      </div>
    );
  }
}


export default withRouter(MessageFormPage);
