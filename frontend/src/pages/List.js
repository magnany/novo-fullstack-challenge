import ListRow from './ListRow';

export default function List(props) {
  const patientRows = props.patients.map((p) => <ListRow key={p._id} patient={p} />);

  return (
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Nom du patient</th>
          <th scope="col">Derniers signes vitaux</th>
          <th scope="col">Fréq. cardiaque</th>
          <th scope="col">Température</th>
          <th scope="col">SpO2</th>
          <th scope="col">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        {patientRows}
      </tbody>
    </table>
  );
}
