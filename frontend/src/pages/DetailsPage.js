import React from 'react';
import axios from 'axios';
import { withRouter } from '../withRouter'
import { Link } from "react-router-dom";
import PatientEssentialsPane from './PatientEssentialsPane'
import PatientLatestVitalsPane from './PatientLatestVitalsPane'
import PatientVitalsStatsPane from './PatientVitalsStatsPane'

class DetailsPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: true, patient: {} }
  }

  async componentDidMount() {
    // À partir du ID de patient...
    const id = this.props.router.params.id;
    // faire l'appel d'API pour aller chercher sa fiche de détails
    const resp = await axios.get(`http://localhost:3001/patient/${id}`);
    // et configurer le state correctement
    if (resp.data.success) {
      this.setState({ loading: false, patient: resp.data.result });
    } else {
      alert(JSON.stringify(resp.data.result));
      this.setState({ loading: false, patient: undefined });
    }
  }

  render() {
    return (
      <div class="mt-3">
        {
          this.state.loading
          ? <h3>Fiche de patient</h3>
          : <h3>{this.state.patient.details.last_name}, {this.state.patient.details.first_name}</h3>
        }
        <Link to="/">Retour à la liste</Link>
        <hr />
        { 
          this.state.loading 
          ? <p>Chargement en cours...</p> 
          : <div class="row">
              { this.state.patient.details && <PatientEssentialsPane patient={this.state.patient.details} /> }
              { this.state.patient.details.vitals.length > 0 && <PatientLatestVitalsPane vitals={this.state.patient.details.vitals} /> }
              { this.state.patient.stats && <PatientVitalsStatsPane stats={this.state.patient.stats} /> }
            </div>
        }
      </div>
    );
  }
}

export default withRouter(DetailsPage);

