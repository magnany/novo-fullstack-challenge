import React from 'react';
import { Link } from 'react-router-dom';
import { calculateAge, formatDate, genderFormat } from '../formatters';

export default function PatientEssentialsPane(props) {
  const patient = props.patient;

  return (
    <div class="col">
      <h5>Données personnelles</h5>
      <ul>
        <li>Âge: {calculateAge(patient.birthdate)} (date de naissance: {formatDate(patient.birthdate)})</li>
        <li>Genre assigné à la naissance: {genderFormat(patient.gender)}</li>
        <li>Courriel: <a href={ "mailto:" + patient.email }>{patient.email}</a></li>
      </ul>
    </div>
  );
}
