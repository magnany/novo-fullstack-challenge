import { heartbeatFormat } from '../formatters';

export default function PatientVitalsStatsPane(props) {
  const stats = props.stats;

  return (
    <div class="col-5">
      <h5>Statistiques</h5>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Donnée</th>
            <th scope="col">Moyenne</th>
            <th scope="col">Min</th>
            <th scope="col">Max</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Fréquence cardiaque</th>
            <td>{heartbeatFormat(stats.heartrateAvg)} bpm</td>
            <td>{stats.heartrateMin} bpm</td>
            <td>{stats.heartrateMax} bpm</td>
          </tr>
          <tr>
            <th scope="row">Température</th>
            <td>{heartbeatFormat(stats.body_tempAvg)} °C</td>
            <td>{stats.body_tempMin} °C</td>
            <td>{stats.body_tempMax} °C</td>
          </tr>
          <tr>
            <th scope="row">SpO2</th>
            <td>{heartbeatFormat(stats.spo2Avg)}</td>
            <td>{stats.spo2Min}</td>
            <td>{stats.spo2Max}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

