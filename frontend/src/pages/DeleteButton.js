import React from 'react';
import axios from 'axios';

export default class DeleteButton extends React.Component {

  constructor(props) {
    super(props);
    this.state = { patient_id: props.patient_id }
    this.delete = this.delete.bind(this);
  }

  async delete(e) {
    e.preventDefault();
    var tag = this;
    axios.delete(`http://localhost:3001/patient/${this.state.patient_id}`)
    .then(function(resp) {
      if (resp.data.success) {
        window.location.reload();
      } else {
        alert(JSON.stringify(resp.data.result));
      }
    }).catch(function(err) {
      alert(JSON.stringify(err));
    });
  }

  render() {
    return (
      <form onSubmit={ this.delete }>
        <button type="submit" class="btn btn-danger btn-sm">Supprimer</button>
      </form>
    );
  }
}

