import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { withRouter } from '../withRouter'

class CreateFormPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      gender: 'U',
      birthdate: '',
      email: ''
    };

    this.firstNameChanged = this.firstNameChanged.bind(this);
    this.lastNameChanged = this.lastNameChanged.bind(this);
    this.genderChanged = this.genderChanged.bind(this);
    this.birthdateChanged = this.birthdateChanged.bind(this);
    this.emailChanged = this.emailChanged.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  componentDidMount() {
    this.navigate = this.props.router.navigate;
  }

  firstNameChanged(e) {
    let value = e.target.value;
    let diff = { first_name: value };
    this.setState(Object.assign({}, this.state, diff));
  }

  lastNameChanged(e) {
    let value = e.target.value;
    let diff = { last_name: value };
    this.setState(Object.assign({}, this.state, diff));
  }

  genderChanged(e) {
    let value = e.target.value;
    let diff = { gender: value };
    this.setState(Object.assign({}, this.state, diff));
  }

  birthdateChanged(e) {
    let value = e.target.value;
    let diff = { birthdate: value };
    this.setState(Object.assign({}, this.state, diff));
  }

  emailChanged(e) {
    let value = e.target.value;
    let diff = { email: value };
    this.setState(Object.assign({}, this.state, diff));
  }

  handleSubmit(e) {
    e.preventDefault();
    var tag = this;
    axios.post('http://localhost:3001/patient', {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      gender: this.state.gender,
      birthdate: this.state.birthdate,
      email: this.state.email
    }).then(function(resp) {
      if (resp.data.success) {
        tag.navigate('/');
      } else {
        alert(JSON.stringify(resp.data.result));
      }
    }).catch(function(error) {
      alert(JSON.stringify(error));
    });
  }

  render() {
    return (
      <div class="mt-3">
        <h3>Créer un patient</h3>
        <Link to="/">Retour à la liste</Link>

        <hr />

        <form onSubmit={ this.handleSubmit }>
          <div class="row mb-3">
            <div class="col">
              <label htmlFor="last_name" class="form-label">Nom</label>
              <input type="text" class="form-control" maxLength="50" name="last_name" value={ this.state.last_name } onChange={ this.lastNameChanged } />
            </div>
            <div class="col">
              <label htmlFor="first_name" class="form-label">Prénom</label>
              <input type="text" class="form-control" maxLength="50" name="first_name" value={ this.state.first_name } onChange={ this.firstNameChanged } />
            </div>
          </div>

          <div class="row mb-3">
            <div class="col">
              <label htmlFor="gender" class="form-label">Genre assigné à la naissance</label>
              <select class="form-select" name="gender" value={ this.state.gender } onChange={ this.genderChanged }>
                <option value="U">Inconnu</option>
                <option value="M">Homme</option>
                <option value="F">Femme</option>
              </select>
            </div>
            <div class="col">
              <label htmlFor="birthdate" class="form-label">Date de naissance</label>
              <input type="date" class="form-control" name="birthdate" value={ this.state.birthdate } onChange={ this.birthdateChanged } />
            </div>
          </div>

          <div class="row mb-3">
            <div class="col">
              <label htmlFor="email" class="form-label">Adresse courriel</label>
              <input type="email" class="form-control" name="email" value={ this.state.email } onChange={ this.emailChanged }/>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Créer le patient</button>
        </form>
      </div>
    );
  }
}

export default withRouter(CreateFormPage);

