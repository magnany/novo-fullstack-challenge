import { Link } from "react-router-dom";
import { formatDateAndTime } from '../formatters';

export default function PatientLatestVitalsPane(props) {
  const vitals = props.vitals;
  
  return (
    <div class="col">
      <h5>Dernières infos</h5>
      <ul>
        <li>Dernière donnée compilée à {formatDateAndTime(vitals[vitals.length - 1].timestamp)}</li>
        <li>Fréquence cardiaque: {vitals[vitals.length - 1].heartrate} bpm</li>
        <li>Température: {vitals[vitals.length - 1].body_temp} °C</li>
        <li>SpO2: {vitals[vitals.length - 1].spo2}</li>
      </ul>
    </div>
  );
}
