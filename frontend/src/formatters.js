
export function calculateAge(birthdate_string) {
  var birthdate = new Date(birthdate_string);
  var monthDiff = Date.now() - birthdate.getTime();
  var ageDate = new Date(monthDiff);
  var year = ageDate.getUTCFullYear();
  var age = Math.abs(year - 1970);
  return age;
}

export function formatDate(date_string) {
  var date = new Date(date_string);
  return date.toISOString().split('T')[0];
}

export function formatDateAndTime(date_string) {
  var date = new Date(date_string);
  return date.toLocaleDateString('fr-CA') + ' ' + date.toLocaleTimeString('fr-CA')
}

export function heartbeatFormat(floatString) {
  return floatString.toFixed(1);
}

export function genderFormat(genderString) {
  if (genderString == "M") {
    return "Homme";
  } else if (genderString == "F") {
    return "Femme";
  } else {
    return "Inconnu";
  }
}
