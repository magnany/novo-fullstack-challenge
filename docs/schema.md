# Schéma de données

## Document _patient_

* **first_name:** Prénom du patient
* **last_name:** Nom du patient
* **birthdate:** Date de naissance du patient afin de dynamiquement pouvoir calculer son âge au lieu de nécessiter une mise à jour chaque année de son âge.
* **gender:** Genre du patient tel que trouvé sur son certificat de naissance. J'ai suivi [la recommendation du CDC pour la collection d'information sur l'identité de genre de patients](https://www.cdc.gov/hiv/clinicians/transforming-health/health-care-providers/collecting-sexual-orientation.html).
* **email:** Adresse courriel du patient.
* **vitals:** Collection de sous-documents _vital_ qui englobe les signes vitaux d'un patient dans notre système.

## Sous-document _vital_

* **timestamp:** Date et heure de collection de ces signes vitaux
* **heartrate:** Pouls (en bpm)
* **body_temp:** Température du corps (en Celsius)
* **spo2:** Saturation périphérique en oxygène (en SpO2)
