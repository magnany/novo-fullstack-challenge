// initialiser l'application express
const express = require('express');
const cors = require('cors');
const port = 3001; // pourrait être déplacé à une variable ENV
const app = express();

app.use(cors());
app.use(express.json());

// configurer les routes de l'API
const patientRoutes = require('./routes/patient.route');
app.use('/patient/', patientRoutes);

// configurer la base de données
const connectionString = "mongodb://127.0.0.1:27017"; // pourrait être déplacé à une variable ENV
const dba = require('./dba');
dba.connect(connectionString, (err) => {
  if (err) {
    console.error(err);
    process.exit()
  }

  // partir le serveur Web
  const server = app.listen(port, (error) => {
    if (error) {
      return console.log(`Error: ${error}`);
    }
    console.log(`Server listening on port ${server.address().port}`);
  });

  // régulièrement générer des données aléatoires pour les patients
  const backgroundInit = require('./background_job');
  backgroundInit();
});

