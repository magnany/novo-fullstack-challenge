const { PatientService } = require('./services/patient.service');
const { heartrate_calc, body_temp_calc, spo2_calc } = require('./vital_calc');

// Modifier ici pour changer la fréquence de mise à jour des signes vitaux aléatoires
const minuteInterval = 1;
const totalMSInterval = minuteInterval * 60 * 1000;

function initializeBackgroundJob() {
  setInterval(() => {
    console.log("Background job starting");
    let patientService = new PatientService();
    patientService.listOfPatients((err, result) => {
      if (!err) {
        result.forEach((patient) => {
          const id = patient._id;
          const now = new Date();
          const newVitals = { 
            timestamp: now, 
            heartrate: heartrate_calc(), 
            body_temp: body_temp_calc(), 
            spo2: spo2_calc()
          }
          patientService.insertVitalsForPatient(id, newVitals, (err2, result2) => {
            if (err2) {
              console.log(`Background job error inserting vitals ${JSON.stringify(newVitals)} for patient ${id}`);
            }
          });
        });
        console.log("Background job ended gracefully");
      } else {
        console.log("Background job error generating list of patients");
      }
    });
  }, totalMSInterval);
}

module.exports = initializeBackgroundJob;

