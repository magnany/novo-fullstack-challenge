const defaultDba = require('../dba');
const { ObjectId } = require('mongodb');

class PatientService {

  // Constructeur accept un database arbitraire pour injection de dépendences
  // (faciliterait les tests)
  constructor(database) {
    this.database = database;
    if (database == undefined) {
      this.database = defaultDba.getDatabase();
    }
  }
  
  // ----- LECTURES -----

  // Requête pour la liste de patients pour le tableau de bord
  listOfPatients(callback) {
    let patients = this.database.collection('patients');

    // éviter de retourner l'historique complet de signes vitaux
    const projection = { vitals: 0 } 
    
    patients.aggregate([
      // trier par nom, prénom, date de naissance
      { $sort : { last_name: 1, first_name: 1, birthdate: 1 } },
      // ajouter un champ qui a les derniers signes vitaux du patient
      { $addFields: { latest_vitals: { $last: "$vitals" } } },  
      // retirer l'historique complet de signes vitaux
      { $project: projection }
    ]).toArray(callback);
  }

  // Requête pour les détails complets d'un patient
  fetchPatientDetails(patientId, successCallback, failureCallback) {
    let patients = this.database.collection('patients');
    const query = { _id: new ObjectId(patientId) }

    // commencer par aller chercher l'ensemble des données du patient
    // Cet appel retourne l'ensemble de l'historique des signes vitaux du patient.
    // Ça serait overkill si l'API était limité à notre interface comme client.
    // Vu que l'intention n'était pas 100% claire, je retourne tout sachant bien qu'on
    // pourrait limiter le subset de données retournées à la dernière donnée ou les 
    // données dans une plage de temps d'intérêt selon la fréquence de mise à jour.
    // Pour les besoins de l'interface, on n'aurait seulement besoin de la dernière 
    // donnée vitale dans la fiche du patient.
    patients.findOne(query, (err, result) => {
      if (err) {
        failureCallback(err);
        return;
      }

      // le patient existe, maintenant on peut calculer les statistiques additionnels
      const aggregatePipeline = [
          { $match: query },                    // limiter aux rangées avec le ID de patient
          { $unwind: "$vitals" },               // dérouler les sous-documents
          { $group: {                           // calculer stats par groupe
            _id: "$_id",
            heartrateAvg: { $avg: "$vitals.heartrate" },
            heartrateMin: { $min: "$vitals.heartrate" },
            heartrateMax: { $max: "$vitals.heartrate" },
            body_tempAvg: { $avg: "$vitals.body_temp" },
            body_tempMin: { $min: "$vitals.body_temp" },
            body_tempMax: { $max: "$vitals.body_temp" },
            spo2Avg: { $avg: "$vitals.spo2" },
            spo2Min: { $min: "$vitals.spo2" },
            spo2Max: { $max: "$vitals.spo2" }
          } }
      ];
      const aggregationCursor = patients.aggregate(aggregatePipeline);

      aggregationCursor.toArray((err2, result2) => {
        if (err2) {
          failureCallback(err2);
        } else {
          // avec les résultats de stats, les combiner dans un objet résultat 
          // qui sera retourné au client de cette méthode

          // le résultat d'aggrégation devrait seulement retourner 1 ou 0 groupes
          // on va prendre le premier pour éviter de gérer un array
          let statsGroup = (result2.length > 0 ? result2[0] : undefined);
          var combined = {
            details: result,
            stats: statsGroup
          };
          successCallback(combined);
        }
      });
    });
  }


  // --- ÉCRITURES / MUTATIONS ---

  // Création d'un document patient
  createPatient(patientDoc, callback) {
    let patients = this.database.collection('patients');
    patients.insertOne(patientDoc, callback);
  }

  // Supprimer un document patient par ID
  deletePatientById(patientId, callback) {
    let patients = this.database.collection('patients');
    const query = { _id: new ObjectId(patientId) }
    patients.deleteOne(query, callback);
  }

  // Insérer un sous-document signe vital à un patient par ID
  insertVitalsForPatient(patientId, newVitals, callback) {
    let patients = this.database.collection('patients');
    const query = { _id: new ObjectId(patientId) }
    
    const updateInstructions = {
      $addToSet: { vitals: newVitals }
    };

    patients.updateOne(query, updateInstructions, callback);
  }

}

module.exports.PatientService = PatientService;
