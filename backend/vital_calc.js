let integerBetween = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let floatBetween = (min, max) => {
  return parseFloat((Math.random() * (max - min) + min).toFixed(1));
}

exports.heartrate_calc = () => {
  const lowBound = 40;
  const highBound = 170;
  return integerBetween(lowBound, highBound);
}

exports.body_temp_calc = () => {
  const lowBound = 36.4;
  const highBound = 37.5;
  return floatBetween(lowBound, highBound);
}

exports.spo2_calc = () => {
  const lowBound = 60;
  const highBound = 100;
  return integerBetween(lowBound, highBound);
}
