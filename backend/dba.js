const { MongoClient } = require('mongodb')

let connection;

module.exports = {
  connect: (connectionString, callback) => {
    const client = new MongoClient(connectionString, { useNewUrlParser: true, useUnifiedTopology: true });

    client.connect((err, db) => {
      if (err || !db) {
        return callback(err);
      }

      connection = db.db("novochallenge");
      return callback();
    });
  },

  getDatabase: () => {
    return connection;
  }
}

