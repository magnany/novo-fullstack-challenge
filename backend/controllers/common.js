exports.success = (res, result) => {
  res.append('Content-Type', 'application/javascript; charset=UTF-8');
  res.json({
    success: true,
    result: result
  });
}

exports.failure = (res, error) => {
  res.append('Content-Type', 'application/javascript; charset=UTF-8');
  res.status(500).json({
    success: false,
    result: error
  });
}

