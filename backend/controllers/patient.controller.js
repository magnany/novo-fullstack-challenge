const { success, failure } = require('./common');
const dba = require('../dba');
const { heartrate_calc, body_temp_calc, spo2_calc } = require('../vital_calc');
const { PatientService } = require('../services/patient.service');
//const nodemailer = require('nodemailer');

// GET: Liste de patients du tableau de bord
exports.list = (req, res, next) => {
  let patientService = new PatientService();
  patientService.listOfPatients((err, result) => {
    if (!err) {
      success(res, result);
    } else {
      failure(res, err);
    }
  });
}

// POST: Création de nouveau patient
exports.create = (req, res, next) => {
  var asDateObject = new Date(req.body.birthdate);
  let patient = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    birthdate: asDateObject,
    gender: req.body.gender,
    email: req.body.email,
    vitals: []
  }
  let patientService = new PatientService();
  patientService.createPatient(patient, (err, result) => {
    if (!err) {
      success(res, result);
    } else {
      failure(res, err);
    }
  });
}

// GET: Fiche de détails du patient (params: id)
exports.details = (req, res, next) => {
  const idParameter = req.params.id;
  let patientService = new PatientService();
  patientService.fetchPatientDetails(idParameter, (result) => {
    success(res, result);
  }, (error) => {
    failure(res, error);
  });
}

// DELETE: Supprimer un patient (params: id)
exports.delete = (req, res, next) => {
  const idParameter = req.params.id;
  let patientService = new PatientService();

  patientService.deletePatientById(idParameter, (err, result) => {
    if (!err) {
      success(res, result);
    } else {
      failure(res, err);
    }
  });
}

// PUT: Mettre à jour les signes vitaux du patient (params: id)
exports.updateVitals = (req, res, next) => {
  const idParameter = req.params.id;
  const now = new Date();
  // les valeurs sont calculées aléatoirement
  // les requis n'étaient pas super clairs sur à qui tombait la responsabilité
  // du calcul aléatoire... je l'ai interprété comme etant la mienne.
  const newVitals = { 
    timestamp: now, 
    heartrate: heartrate_calc(), 
    body_temp: body_temp_calc(), 
    spo2: spo2_calc()
  }

  // insertion du sous-document via le service
  let patientService = new PatientService();
  patientService.insertVitalsForPatient(idParameter, newVitals, (err, result) => {
    if (!err) {
      success(res, result);
    } else {
      failure(res, err);
    }
  });
}

// POST: Envoyer un message au patient (params: id)
// COUPÉ PAR MANQUE DE TEMPS
/*
exports.sendMessage = (req, res, next) => {
  let patients = dba.getDatabase().collection('patients');
  const idParameter = req.params.id;
  const query = { _id: new ObjectId(idParameter) }

  patients.findOne(query, (err, result) => {
    if (!err) {
      const email = result.email;

      nodemailer.createTestAccount((err2, acct) => {
        if (err2) {
          failure(res, err2);
          return;
        }

        let transporter = nodemailer.createTransport({
          host: 'smtp.ethereal.email',
          port: 587,
          secure: false,
          auth: { user: testAccount.user, pass: testAccount.pass }
        });

        let msg = {
          from: 'Novo Challenge <yanik.magnan@gmail.com>',
          to: `${result.first_name} ${result.last_name} <${result.email}>`,
          subject: 'Vous avez reçu un nouveau message de votre docteur!',
          text: req.body.message
        };

        transporter.sendMail(msg, (err3, info) => {
          if (err3) {
            failure(res, err3);
            return;
          }

          success(res, info);
        });
      });
    } else {
      failure(res, err);
    }
  });
}
*/
