const express = require('express');
const PatientController = require('../controllers/patient.controller');

var router = express.Router();

// Liste de patients du tableau de bord
router.get('/', PatientController.list);

// Création de nouveau patient
router.post('/', PatientController.create);

// Fiche de détails du patient
router.get('/:id', PatientController.details);

// Supprimer un patient
router.delete('/:id', PatientController.delete);

// Mettre à jour les signes vitaux du patient
router.put('/:id/vitals', PatientController.updateVitals);

// Envoyer un message au patient
// COUPÉ PAR MANQUE DE TEMPS
// router.post('/:id/messages', PatientController.sendMessage);

module.exports = router;
